/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.autokomis.servlets;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.autokomis.ConfigHibernate;
import pl.sda.autokomis.model.Customer;

/**
 *
 * @author artisticImpresion
 */
public class ListCars {
    
    public List<Customer> getListOfCustomers() {
        
        SessionFactory instance = ConfigHibernate.getInstance();
        List<Customer> list = new ArrayList<>();
        Session openSession = instance.openSession();
        Transaction tx = null;
        try {
            tx = openSession.beginTransaction();
             list = openSession.createQuery("from Customer Where id =(select max(id) from Customer)").list(); //  from Owner Order By id Desc

            } catch (Exception err) {
            System.out.println(err.getMessage());
            } finally {
            openSession.close();
            }
            
            return list;
            
    }
    
}
