/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.autokomis.servlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.autokomis.ConfigHibernate;
import pl.sda.autokomis.model.Car;
import pl.sda.autokomis.model.Customer;

/**
 *
 * @author artisticImpresion
 */
@WebServlet
public class AddCar extends HttpServlet {
    
    private SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //nothing here
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/AddCar").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String peselNip = request.getParameter("peselNip");
        String phoneNo = request.getParameter("phoneNo");
        
        String vinNo = request.getParameter("vinNo");
        String regNo = request.getParameter("regNo");
        String prodYear = request.getParameter("prodYear");
        String firstRegDate = request.getParameter("firstRegDate");
        String make = request.getParameter("make");
        String model = request.getParameter("model");
        String insuranceNo = request.getParameter("insuranceNo");
        String regDocumentNo = request.getParameter("regDocumentNo");
        String fuelType = request.getParameter("fuelType");
        String mileage = request.getParameter("mileage");
        String engine = request.getParameter("engine");
        String horsePower = request.getParameter("horsePower");
        String gearType = request.getParameter("gearType");
        String description = request.getParameter("description");
        String price = request.getParameter("price");

        SessionFactory instance = ConfigHibernate.getInstance();
        Session openSession = instance.openSession();
        Transaction trans = openSession.beginTransaction();
        try {
            Customer customerToAdd = new Customer();
            customerToAdd.setName(name);
            customerToAdd.setAddress(address);
            customerToAdd.setPeselNip(peselNip);
            customerToAdd.setPhoneNo(Integer.parseInt(phoneNo));
            openSession.save(customerToAdd);
      

            Car carToAdd = new Car();
            carToAdd.setVinNo(vinNo);
            carToAdd.setRegNo(regNo);
            carToAdd.setProdYear(Integer.parseInt(prodYear));
            carToAdd.setFirstRegDate(myDateFormat.parse(firstRegDate));

            carToAdd.setMake(make);
            carToAdd.setModel(model);
            carToAdd.setInsuranceNo(insuranceNo);
            carToAdd.setRegDocumentNo(regDocumentNo);
            carToAdd.setFuelType(fuelType);
            carToAdd.setMileage(Integer.parseInt(mileage));
            carToAdd.setEngine(engine);
            carToAdd.setHorsePower(Integer.parseInt(horsePower));
            carToAdd.setGearType(gearType);
            carToAdd.setDescription(description);
            carToAdd.setNumberOfTestDrives(0);
            BigDecimal bigPrice;
            bigPrice = new BigDecimal(price);
            carToAdd.setPrice(bigPrice);
            carToAdd.setStatus("nowy");
            carToAdd.setCustomer(customerToAdd);

            openSession.save(carToAdd);

                trans.commit();
            } catch (Exception e) {
                trans.rollback();
            }
            openSession.close();        
        
        //testowe przekierowanie
        request.getRequestDispatcher("/index.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
