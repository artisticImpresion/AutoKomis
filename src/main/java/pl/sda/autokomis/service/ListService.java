/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.autokomis.service;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.autokomis.ConfigHibernate;
import pl.sda.autokomis.model.Car;
import pl.sda.autokomis.model.Customer;

/**
 *
 * @author artisticImpresion
 */
public class ListService {
    
    public List<Customer> getListOfCustomers() {

        SessionFactory instance = ConfigHibernate.getInstance();
        List<Customer> list = new ArrayList<>();
        Session newSession = instance.openSession();
        Transaction tx = null;
        try {
            tx = newSession.beginTransaction();
            list = newSession.createQuery("from Customer").list(); //  from Owner Order By id Desc

        } catch (Exception err) {
            System.out.println(err.getMessage());
        } finally {
            newSession.close();
        }
        return list;
    }
    
//    public List<Customer> getLastCustomer() {
//        SessionFactory instance = ConfigHibernate.getInstance();
//        List<Customer> list = new ArrayList<>();
//        Session newSession = instance.openSession();
//        Transaction trans = null;
//        try {
//            trans = newSession.beginTransaction();
//            list = newSession.createQuery("from Customer Where id =(select max(id) from Customer)").list();
//        } catch (Exception err) {
//            System.out.println(err.getMessage());
//        } finally {
//            newSession.close();
//        }        
//        return list;
//
//    }
    
    public List<Car> getListOfCars() {

        SessionFactory instance = ConfigHibernate.getInstance();
        List<Car> list = new ArrayList<>();
        Session newSession = instance.openSession();
        Transaction tx = null;
        try {
            tx = newSession.beginTransaction();
            list = newSession.createQuery("from Car").list(); 

        } catch (Exception err) {
            System.out.println(err.getMessage());
        } finally {
            newSession.close();
        }
        return list;
    }
    
    
    
}
