<%-- 
    Document   : display
    Created on : 2017-08-08, 12:36:41
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12" style="min-height: 600px; border: #000000 solid 1px;">
        
        <c:choose>
            <c:when test="${param.menu == 'home'}"><%@include file="/WEB-INF/home.jsp" %></c:when>
            
            <c:when test="${param.menu == 'cars'}"><%@include file="/WEB-INF/cars.jsp" %></c:when>
                <c:when test="${param.menu == 'addNewCar'}"><%@include file="/WEB-INF/add_new_car.jsp" %></c:when>
                <c:when test="${param.menu == 'showAllCars'}"><%@include file="/WEB-INF/show_all_cars.jsp" %></c:when>            
            <c:when test="${param.menu == 'contact'}"><%@include file="/WEB-INF/contact.jsp" %></c:when>
            
            <c:when test="${param.menu == 'employee'}"><%@include file="/WEB-INF/employee_panel.jsp" %></c:when>
                <c:when test="${param.menu == 'showAllCustomers'}"><%@include file="/WEB-INF/show_all_customers.jsp" %></c:when>
                <c:when test="${param.menu == 'showCarOwners'}"><%@include file="/WEB-INF/show_car_owners.jsp" %></c:when>

            <c:when test="${param.menu == 'administrator'}"><%@include file="/WEB-INF/admin_panel.jsp" %></c:when>
                <c:when test="${param.menu == 'changeCarStatus'}"><%@include file="/WEB-INF/change_car_status.jsp" %></c:when>
                
            <c:otherwise><%@include file="/WEB-INF/home.jsp" %></c:otherwise>
        </c:choose>
    </div>
</div>