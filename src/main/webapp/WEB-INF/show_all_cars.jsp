<%-- 
    Document   : show_all_cars
    Created on : 2017-08-09, 17:50:13
    Author     : artisticImpresion
--%>

<%@page import="pl.sda.autokomis.service.ListService"%>
<%@page import="java.util.List"%>
<%@page import="pl.sda.autokomis.model.Customer"%>
<%@page import="pl.sda.autokomis.model.Car"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Oto nasza oferta:</h3>
<br/>
<table class="table table-sm table-bordered table-striped">
    <thead class="thead-inverse">
        <tr class="bg-info">
            <th>ID</th><th>Make</th><th>Model</th><th>Engine <br/>capacity</th><th>Mileage</th><th>Price</th><th>Description</th>
    </tr>
    </thead>
    <tbody>
    <%   ListService listCars = new ListService();
List<Car> list = listCars.getListOfCars();
String toyota = "Toyota";
for (Car c : list) {
    String testMake = c.getMake();
    %>
    <tr class="bg-info table-info">
                <td> <%=c.getId()%> </td>
                <td>
                    
                    <% if(testMake.equals(toyota)){ %>
                        <img src="img/logo/toyota.200.png"/>
                    <% } else {%>
                    <%=testMake%>
                    <% } %>
                </td>
                <td> <%=c.getModel()%> </td>
                <td> <%=c.getEngine()%> </td>
                <td> <%=c.getMileage()%> </td>
                <td> <%=c.getPrice()%> </td>
                <td> <%=c.getDescription()%> </td>

            </tr>
    <%}%>
    </tbody>
</table>    
     

