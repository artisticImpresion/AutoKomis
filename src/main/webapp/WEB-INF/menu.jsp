<%-- 
    Document   : menu
    Created on : 2017-08-08, 13:25:08
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ul class="nav nav-pills nav-stacked">
    <li role="presentation" class="<c:if test="${param.menu=='home'}"><c:out value="active"></c:out></c:if>"><a href="index.jsp?menu=home">Home</a></li>
    <li role="presentation" class="<c:if test="${param.menu=='cars'}"><c:out value="active"></c:out></c:if>"><a href="index.jsp?menu=cars">Cars</a></li>
    <li role="presentation" class="<c:if test="${param.menu=='contact'}"><c:out value="active"></c:out></c:if>"><a href="index.jsp?menu=contact">Contact</a></li>
    <li role="presentation" class="<c:if test="${param.menu=='employee'}"><c:out value="active"></c:out></c:if>"><a href="index.jsp?menu=employee">Employee</a></li>
    <li role="presentation" class="<c:if test="${param.menu=='administrator'}"><c:out value="active"></c:out></c:if>"><a href="index.jsp?menu=administrator">Admin</a></li>
    
</ul>
