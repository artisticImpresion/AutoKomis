<%-- 
    Document   : show_car_owners
    Created on : 2017-08-10, 18:05:29
    Author     : artisticImpresion
--%>

<%@page import="pl.sda.autokomis.service.ListService"%>
<%@page import="java.util.List"%>
<%@page import="pl.sda.autokomis.model.Customer"%>
<%@page import="pl.sda.autokomis.model.Car"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Oto pełna lista właścicieli samochodów:</h3>
<br/>
<table class="table table-sm table-bordered table-striped">
    <thead class="thead-inverse">
        <tr class="bg-info">
            <th>ID <br/>właściciela</th>
            <th>Nazwa</th>
            <th>Numer <br/>telefonu</th>
            <th>Marka <br/>samochodu</th>
            <th>Model <br/>samochodu</th>
            <th>Numer <br/>rejestracyjny</th>
            <th>VIN <br/>samochodu</th>
            <th>Rocznik <br/>produkcji</th>
            <th>Cena <br/>(PLN)</th>

        </tr>
    </thead>
    <tbody>
        <%   ListService listCars = new ListService();
            List<Car> list = listCars.getListOfCars();
            for (Car c : list) {
        %>
        <tr class="bg-info table-info">
            <td> <%=c.getCustomer().getId()%> </td>
            <td> <%=c.getCustomer().getName()%> </td>
            <td> <%=c.getCustomer().getPhoneNo()%> </td>
            <td> <%=c.getMake()%> </td>
            <td> <%=c.getModel()%> </td>
            <td> <%=c.getRegNo()%> </td>
            <td> <%=c.getVinNo()%> </td>
            <td> <%=c.getProdYear()%> </td>
            <td> <%=c.getPrice()%> </td>

        </tr>
        <%}%>
    </tbody>
</table>    