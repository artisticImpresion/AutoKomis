<%-- 
    Document   : change_car_status
    Created on : 2017-08-10, 19:42:31
    Author     : artisticImpresion
--%>

<%@page import="pl.sda.autokomis.service.ListService"%>
<%@page import="java.util.List"%>
<%@page import="pl.sda.autokomis.model.Customer"%>
<%@page import="pl.sda.autokomis.model.Car"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Opcja zmiany statusu samochodu:</h3>
<br/>
<table class="table table-sm table-bordered table-striped">
    <thead class="thead-inverse">
        <tr class="bg-info">
            <th>ID</th>
            <th>Status <br/>pojazdu</th>
            <th>Marka</th>
            <th>Model</th>
            <th>Numer <br/>rejestracyjny</th>
            <th>Numer <br/>VIN</th>
            <th>Pojemność <br/>silnika</th>
            <th>Przebieg</th>
            <th>Cena</th>
        </tr>
    </thead>
    <tbody>
        <%   ListService listCars = new ListService();
            List<Car> list = listCars.getListOfCars();
            for (Car c : list) {
        %>
        <tr class="bg-info table-info">
            <td> <%=c.getId()%> </td>
            <td> <%=c.getStatus()%> <br/>
                <form method="POST" action="<%=request.getContextPath()%>/index.jsp?menu=changeCarStatus">
                    <select name="chooseStatus" id="<%=c.getId()%>">
                        <option value="nowy" selected>nowy</option>
                        <option value="wystawiony">wystawiony</option>
                        <option value="sprzedany">sprzedany</option>
                        <option value="wycofany">wycofany</option>
                    </select><br/>
                    <input type="submit" value="Zmień"/><br/>
                </form>
            </td>
            <td> <%=c.getMake()%> </td>
            <td> <%=c.getModel()%> </td>
            <td> <%=c.getRegNo()%> </td>
            <td> <%=c.getVinNo()%> </td>
            <td> <%=c.getEngine()%> </td>
            <td> <%=c.getMileage()%> </td>
            <td> <%=c.getPrice()%> </td>
        </tr>
        <%}%>
    </tbody>
</table>
