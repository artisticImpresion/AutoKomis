<%-- 
    Document   : add_new_car
    Created on : 2017-08-09, 10:41:08
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<h2>Add your car below:</h2>
<br/>
<form method="POST" action="<%=request.getContextPath()%>/AddCar">
    <label>Imię i Nazwisko / Nazwa Firmy: <input type="text" name="name" /></label><br/>
    <label>Adres korespondencyjny: <input type="text" name="address" /></label><br/>
    <label>PESEL/NIP: <input type="text" name="peselNip" /></label><br/>
    <label>Numer telefonu: <input type="text" name="phoneNo" /></label><br/>
    <br/>
    <p>Samochód:</p>
    <label>Numer VIN: <input type="text" name="vinNo" /></label><br/>
    <label>Numer rejestracyjny: <input type="text" name="regNo" /></label><br/>
    <label>Rok produkcji: <input type="text" name="prodYear" /></label><br/>
    <label>Data pierwszej rejestracji: <input type="text" name="firstRegDate" placeholder="dd-mm-rrrr" /></label><br/>
    <label>Marka: <input type="text" name="make" /></label><br/>
    <label>Model: <input type="text" name="model" /></label><br/>
    <label>Numer ubezpieczenia: <input type="text" name="insuranceNo" /></label><br/>
    <label>Numer dowodu rejestracyjnego: <input type="text" name="regDocumentNo" /></label><br/> 
    <label>Typ paliwa: <input type="text" name="fuelType" /></label><br/>
    <label>Przebieg: <input type="text" name="mileage" /></label><br/>
    <label>Pojemność silnika (w cm3): <input type="text" name="engine" /></label><br/>
    <label>Moc silnika (w KM): <input type="text" name="horsePower" /></label><br/>
    <label>Typ skrzyni biegów: <input type="text" name="gearType" /></label><br/>
    <label>Opis pojazdu: <input type="text" name="description" /></label><br/>
    <label>Cena: <input type="text" name="price" /></label><br/>
    <br/>
    <input type="reset" class="btn btn-primary" value="Wyczyść dane"/> lub 
    <input type="submit" class="btn btn-primary" value="Wyślij formularz"/><br/>
</form>
