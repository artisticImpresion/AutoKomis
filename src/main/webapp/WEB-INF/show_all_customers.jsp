<%-- 
    Document   : show_all_customers
    Created on : 2017-08-10, 13:11:24
    Author     : artisticImpresion
--%>

<%@page import="pl.sda.autokomis.service.ListService"%>
<%@page import="java.util.List"%>
<%@page import="pl.sda.autokomis.model.Customer"%>
<%@page import="pl.sda.autokomis.model.Car"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Oto pełna lista naszych klientów:</h3>
<br/>
<table class="table table-sm table-bordered table-striped">
    <thead class="thead-inverse">
        <tr class="bg-info">
            <th>ID</th><th>Name</th><th>Address</th><th>Phone Number</th><th>PESEL / NIP</th>
        </tr>
    </thead>
    <tbody>
        <%   ListService listCustomers = new ListService();
            List<Customer> list = listCustomers.getListOfCustomers();

            for (Customer c : list) {
        %>
        <tr class="bg-info table-info">
            <td> <%=c.getId()%> </td>
            <td> <%=c.getName()%> </td>
            <td> <%=c.getAddress()%> </td>
            <td> <%=c.getPhoneNo()%> </td>
            <td> <%=c.getPeselNip()%> </td>
        </tr>
        <%}%>
    </tbody>
</table>    


