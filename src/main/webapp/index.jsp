<%-- 
    Document   : index
    Created on : 2017-08-08, 11:08:40
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <%@include file="/WEB-INF/head.jsp" %>
  <body>
    <div class="container">
        <%@include file="/WEB-INF/header.jsp" %>
        
        <div class="row">
            <div class="col-md-2">
                <%@include file="/WEB-INF/left_column.jsp" %>
            </div>
            <div class="col-md-10">
                <%@include file="/WEB-INF/display.jsp" %>
            </div>
        </div>

        <%@include file="/WEB-INF/footer.jsp" %>
    </div><!-- container end -->  
  </body>
</html>
